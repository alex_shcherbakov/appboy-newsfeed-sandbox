//
//  AppDelegate.h
//  Sandbox
//
//  Created by Alexander Shcherbakov on 12/1/15.
//  Copyright © 2015 Alexander Shcherbakov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

