//
//  AppDelegate.m
//  Sandbox
//
//  Created by Alexander Shcherbakov on 12/1/15.
//  Copyright © 2015 Alexander Shcherbakov. All rights reserved.
//

#import "AppDelegate.h"
#import "NUISettings.h"

#import <AppboyKit.h>

@interface AppDelegate ()

@end

static NSString *const AppboyApiKey = @"appboy-sample-ios";

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    // Starts up Appboy, opening a new session and causing an updated in-app message/feed to be requested.
    [Appboy startWithApiKey:AppboyApiKey
              inApplication:application
          withLaunchOptions:launchOptions
          withAppboyOptions:@{ABKRequestProcessingPolicyOptionKey: @(ABKAutomaticRequestProcessing)}];
    
    // Enable/disable Appboy to use NUI theming. Try turning it on and off to see the results!  (Look at the Appboy
    // feedback form and news feed).
    [NUISettings initWithStylesheet:@"BaubleBarNUIStyle"];
    [Appboy sharedInstance].useNUITheming = YES;
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
