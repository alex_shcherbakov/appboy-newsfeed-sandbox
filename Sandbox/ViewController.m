//
//  ViewController.m
//  Sandbox
//
//  Created by Alexander Shcherbakov on 12/1/15.
//  Copyright © 2015 Alexander Shcherbakov. All rights reserved.
//

#import "ViewController.h"

#import <Appboy-iOS-SDK/AppboyKit.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupDesign];
}

- (void)setupDesign
{
    ABKFeedViewControllerGenericContext *newsFeedController = [[ABKFeedViewControllerGenericContext alloc] init];
    [self addChildViewController:newsFeedController];
    newsFeedController.view.frame = self.view.frame;
    [self.view addSubview:newsFeedController.view];
    [newsFeedController didMoveToParentViewController:self];
}

@end
