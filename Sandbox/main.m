//
//  main.m
//  Sandbox
//
//  Created by Alexander Shcherbakov on 12/1/15.
//  Copyright © 2015 Alexander Shcherbakov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
